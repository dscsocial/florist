package id.ac.darmajaya.florist;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import id.ac.darmajaya.florist.Model.Florist;
import id.ac.darmajaya.florist.Model.Produk;
import id.ac.darmajaya.florist.Model.User;

public class HistoryDetail extends AppCompatActivity {

    private TextView namatoko, alamatoko, paket, status, alamatpenerima, tanggalpengiriman, deskripsi;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_detail_activity);

        namatoko = (TextView) findViewById(R.id.namatoko);
        alamatoko = (TextView) findViewById(R.id.alamattoko);
        paket = (TextView) findViewById(R.id.paket);
        status = (TextView) findViewById(R.id.status);
        alamatpenerima = (TextView) findViewById(R.id.alamatpenerima);
        tanggalpengiriman = (TextView) findViewById(R.id.tanggalpengiriman);
        deskripsi = (TextView) findViewById(R.id.deskripsi);

        alamatpenerima.setText(getIntent().getStringExtra("ALAMATTUJUAN"));
        deskripsi.setText(getIntent().getStringExtra("DESKRIPSI"));

        db = FirebaseFirestore.getInstance();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");

        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        showtoko(getIntent().getStringExtra("IDTOKO"));
        showstatus(getIntent().getStringExtra("STATUS"));
        showkirim(getIntent().getStringExtra("TANGGALPENGIRIMAN"));
        showfotokarangan(getIntent().getStringExtra("FOTOKARANGAN"));


        ((Button) findViewById(R.id.tampilkan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogImageFull(getIntent().getStringExtra("FOTOBUKTI"));
            }
        });

    }

    private void showtoko(String data){

        DocumentReference toko = db.collection("toko").document(data);
        DocumentReference produktoko = db.collection("toko").document(data).collection("produk").document(getIntent().getStringExtra("IDBARANG"));

        toko.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Florist toko = document.toObject(Florist.class);
                    namatoko.setText(toko.getNamatoko());
                    alamatoko.setText(toko.getAlamat());
                } else {

                }
            }
        });

        produktoko.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Produk toko = document.toObject(Produk.class);
                    paket.setText(toko.getNamaproduk());
                } else {

                }
            }
        });

    }

    private void showkirim (String tanggalkirim){
        if (tanggalkirim.equals("")){
            tanggalpengiriman.setText("-");

        }
        else
        {
            tanggalpengiriman.setText(tanggalkirim);


        }

    }

    private void showstatus(String paket){

        if (paket.equals("1")){
            status.setText("Transfer Belum Diverifikasi");

        }
        else if (paket.equals("2")){
            status.setText("Pesanan Sedang Diproses");
        }

        else if (paket.equals("3")){
            status.setText("Orderan Selesai");

        }
        else
        {
            status.setText("Belum Transfer");
        }

    }

    private void showfotokarangan (String url){
        if (url.equals("")){

        }
        else
        {
            ImageView foto = (ImageView) findViewById(R.id.image);
            Picasso.get()
                    .load(url)
                    .into(foto);
        }


    }

    private void showDialogImageFull(String url) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_image);
        ImageView foto = dialog.findViewById(R.id.transfer);

            Picasso.get()
                    .load(url)
                    .into(foto);

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

}
