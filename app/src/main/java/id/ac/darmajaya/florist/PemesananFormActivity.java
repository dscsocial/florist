package id.ac.darmajaya.florist;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import id.ac.darmajaya.florist.Model.Transaksi;
import id.ac.darmajaya.florist.Model.User;


public class PemesananFormActivity extends AppCompatActivity {


    public FirebaseFirestore db;
    private TextView username, usernametlp, namaproduk, harga;
    private EditText etdeskripsi, namapenerima, alamatpenerima;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pemesanan_activity);
        username = (TextView) findViewById(R.id.username);
        usernametlp = (TextView) findViewById(R.id.usernotelp);
        namaproduk = (TextView) findViewById(R.id.namaproduk);
        harga = (TextView) findViewById(R.id.harga);
        etdeskripsi = (EditText) findViewById(R.id.deskripsi);
        alamatpenerima = (EditText) findViewById(R.id.alamatpenerima);
        submit = (Button) findViewById(R.id.submit);
        db = FirebaseFirestore.getInstance();




        SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);

        namaproduk.setText(sp.getString("NAMABARANG", null));
        long data = sp.getLong("HARGA", 0);

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        harga.setText(formatRupiah.format(Double.parseDouble(String.valueOf(data))));



        DocumentReference docRef = db.collection("user").document(sp.getString("USERID", null));
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                User user = documentSnapshot.toObject(User.class);
                username.setText(user.getNama());
                usernametlp.setText(user.getNotelp());
            }
        });





        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);

                String idtoko = sp.getString("IDTOKO", null);
                String idbarang = sp.getString("IDBARANG", null);
                String iduser = sp.getString("USERID", null);
                String kordinat = sp.getString("KORDINAT", null);
                String deskripsi = etdeskripsi.getText().toString();
                String alamattujuan = alamatpenerima.getText().toString();

                Transaksi transaksi = new Transaksi();
                transaksi.setIdtoko(idtoko);
                transaksi.setIdbarang(idbarang);
                transaksi.setIduser(iduser);
                transaksi.setKordinat(kordinat);
                transaksi.setDeskripsi(deskripsi);
                transaksi.setAlamattujuan(alamattujuan);
                transaksi.setFotokarangan("");
                transaksi.setFototransaksi("");
                transaksi.setStatus("");
                transaksi.setTanggalpengiriman("");

    /*            Map<String, Object> datatransaksi = new HashMap<>();
                datatransaksi.put("idtoko", idtoko);
                datatransaksi.put("idbarang", idbarang);
                datatransaksi.put("iduser", iduser);
                datatransaksi.put("kordinat", kordinat);
                datatransaksi.put("deskripsi", deskripsi);
                datatransaksi.put("alamattujuan", alamattujuan);*/

                db.collection("transaksi").document()
                        .set(transaksi)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toasty.success(getApplicationContext(), "Pemesanan Berhasil", Toast.LENGTH_LONG).show();
                                finish();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });
            }
        });



    }
}
