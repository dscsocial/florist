package id.ac.darmajaya.florist;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import id.ac.darmajaya.florist.Model.Produk;
import id.ac.darmajaya.florist.utils.Tools;
import id.ac.darmajaya.florist.widget.SpacingItemDecoration;

import static id.ac.darmajaya.florist.MapsLayout.MapsPengiriman.KOORDINAT;

public class DaftarProdukActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FirestoreRecyclerAdapter<Produk, DaftarProdukActivity.MyViewHolder> adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daftar_produk_activity);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 3), true));
        recyclerView.setHasFixedSize(true);

        starter();
    }



    public void starter(){
        SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);

        Query query = FirebaseFirestore.getInstance()
                .collection("toko").document(sp.getString("IDTOKO", null)).collection("produk");

        FirestoreRecyclerOptions<Produk> options = new FirestoreRecyclerOptions.Builder<Produk>()
                .setQuery(query, Produk.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Produk, DaftarProdukActivity.MyViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull DaftarProdukActivity.MyViewHolder holder, int position, @NonNull final Produk model) {
                holder.setImage(model.getFoto());
                holder.setTitle(model.getNamaproduk());
                long harga = model.getHarga();

                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                holder.setHarga(formatRupiah.format(Double.parseDouble(String.valueOf(harga))));

                String id = adapter.getSnapshots().getSnapshot(position).getId();
                model.setIdproduk(id);

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), DetailProdukActivity.class);
                        intent.putExtra("GAMBAR", model.getFoto());
                        intent.putExtra("NAMA", model.getNamaproduk());
                        intent.putExtra("DESKRIPSI", model.getDetail());
                        intent.putExtra("HARGA", model.getHarga());

                        SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("IDBARANG", model.getIdproduk());
                        editor.putString("NAMABARANG", model.getNamaproduk());
                        editor.putLong("HARGA", model.getHarga());
                        editor.commit();

                        startActivity(intent);


                    }
                });

            }



            @NonNull
            @Override
            public DaftarProdukActivity.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.daftar_produk_rcv, parent, false);
                return new DaftarProdukActivity.MyViewHolder(view);
            }
        };

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

        }


        public void setTitle(String name) {
            TextView title = mView.findViewById(R.id.name);
            title.setText(name);
        }


        public void setHarga(String harga) {
            TextView hargabarang = mView.findViewById(R.id.harga);
            hargabarang.setText(harga);
        }


        public void setImage(String image) {
            ImageView gambar = mView.findViewById(R.id.image);
            Picasso.get()
                    .load(image)
                    .into(gambar);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
}
