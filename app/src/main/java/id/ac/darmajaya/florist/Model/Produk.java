package id.ac.darmajaya.florist.Model;

public class Produk {

    private String idproduk;
    private String namaproduk;
    private long harga;
    private String foto;
    private String detail;

    public Produk(String idproduk, String namaproduk, long harga, String foto, String detail) {
        this.idproduk = idproduk;
        this.namaproduk = namaproduk;
        this.harga = harga;
        this.foto = foto;
        this.detail = detail;
    }

    public Produk(){

    }

    public String getIdproduk() {
        return idproduk;
    }

    public void setIdproduk(String idproduk) {
        this.idproduk = idproduk;
    }

    public String getNamaproduk() {
        return namaproduk;
    }

    public void setNamaproduk(String namaproduk) {
        this.namaproduk = namaproduk;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }



}


