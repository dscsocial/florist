package id.ac.darmajaya.florist.Model;

public class Florist {

    private String iddokumen;
    private String namatoko;
    private String alamat;
    private String notelepon;
    private String kordinat;
    private String foto;
    private String harga;

    public Florist(){

    }


    public Florist(String iddokumen, String namatoko, String alamat, String notelepon, String kordinat, String foto, String harga) {
        this.iddokumen = iddokumen;
        this.namatoko = namatoko;
        this.alamat = alamat;
        this.notelepon = notelepon;
        this.kordinat = kordinat;
        this.foto = foto;
        this.harga = harga;
    }

    public String getIddokumen() {
        return iddokumen;
    }

    public void setIddokumen(String iddokumen) {
        this.iddokumen = iddokumen;
    }

    public String getNamatoko() {
        return namatoko;
    }

    public void setNamatoko(String namatoko) {
        this.namatoko = namatoko;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNotelepon() {
        return notelepon;
    }

    public void setNotelepon(String notelepon) {
        this.notelepon = notelepon;
    }

    public String getKordinat() {
        return kordinat;
    }

    public void setKordinat(String kordinat) {
        this.kordinat = kordinat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }


}
