package id.ac.darmajaya.florist.Model;

public class Transaksi {
    private String idtoko;
    private String idbarang;
    private String iduser;
    private String kordinat;
    private String deskripsi;
    private String alamattujuan;
    private String fototransaksi;
    private String status;
    private String fotokarangan;
    private String tanggalpengiriman;



    public Transaksi(String idtoko, String idbarang, String iduser, String kordinat, String deskripsi, String alamattujuan, String fototransaksi, String status, String fotokarangan, String tanggalpengiriman) {
        this.idtoko = idtoko;
        this.idbarang = idbarang;
        this.iduser = iduser;
        this.kordinat = kordinat;
        this.deskripsi = deskripsi;
        this.alamattujuan = alamattujuan;
        this.fototransaksi = fototransaksi;
        this.status = status;
        this.fotokarangan = fotokarangan;
        this.tanggalpengiriman = tanggalpengiriman;
    }

    public Transaksi (){

    }

    public String getIdtoko() {
        return idtoko;
    }

    public void setIdtoko(String idtoko) {
        this.idtoko = idtoko;
    }

    public String getIdbarang() {
        return idbarang;
    }

    public void setIdbarang(String idbarang) {
        this.idbarang = idbarang;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getKordinat() {
        return kordinat;
    }

    public void setKordinat(String kordinat) {
        this.kordinat = kordinat;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getAlamattujuan() {
        return alamattujuan;
    }

    public void setAlamattujuan(String alamattujuan) {
        this.alamattujuan = alamattujuan;
    }

    public String getFototransaksi() {
        return fototransaksi;
    }

    public void setFototransaksi(String fototransaksi) {
        this.fototransaksi = fototransaksi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFotokarangan() {
        return fotokarangan;
    }

    public void setFotokarangan(String fotokarangan) {
        this.fotokarangan = fotokarangan;
    }

    public String getTanggalpengiriman() {
        return tanggalpengiriman;
    }

    public void setTanggalpengiriman(String tanggalpengiriman) {
        this.tanggalpengiriman = tanggalpengiriman;
    }

}
