package id.ac.darmajaya.florist;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import id.ac.darmajaya.florist.Model.Florist;

import static id.ac.darmajaya.florist.MapsLayout.MapsPengiriman.KOORDINAT;

public class RadiusActivity extends AppCompatActivity {


    private FirestoreRecyclerAdapter<Florist, RadiusActivity.MyViewHolder> adapter;
    RecyclerView recyclerview;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radius_activity);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerV);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        
        starter();


    }

    public void starter(){
        Query query = FirebaseFirestore.getInstance()
                .collection("toko");


        FirestoreRecyclerOptions<Florist> options = new FirestoreRecyclerOptions.Builder<Florist>()
                .setQuery(query, Florist.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Florist, RadiusActivity.MyViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RadiusActivity.MyViewHolder holder, int position, @NonNull final Florist model) {
                String data = model.getKordinat();
                String intent = getIntent().getStringExtra(KOORDINAT);

                intent = intent.replaceAll("\\(", "").replaceAll("\\)","").replaceAll("lat/lng:", "").replaceAll(" ", "");
                final String[] data1 = intent.split(",");
                String[] data2 = data.split(",");
                LatLng l1=new LatLng(Double.parseDouble(data1[0]), Double.parseDouble(data1[1]));
                LatLng l2=new LatLng(Double.parseDouble(data2[0]), Double.parseDouble(data2[1]));


                String id = adapter.getSnapshots().getSnapshot(position).getId();
                model.setIddokumen(id);

                Log.i("Dokumen", id);


                holder.setJarak(String.valueOf(getDistance(l1, l2)));
                holder.setTitle(model.getNamatoko());


                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), DaftarProdukActivity.class);
                        SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("IDTOKO", model.getIddokumen());
                        editor.commit();
                        startActivity(intent);

                    }
                });

            }


            @NonNull
            @Override
            public RadiusActivity.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.radius_rcv, parent, false);
                return new RadiusActivity.MyViewHolder(view);
            }
        };

        recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private String getDistance(LatLng my_latlong, LatLng frnd_latlong){
        Location l1=new Location("One");
        l1.setLatitude(my_latlong.latitude);
        l1.setLongitude(my_latlong.longitude);

        Location l2=new Location("Two");
        l2.setLatitude(frnd_latlong.latitude);
        l2.setLongitude(frnd_latlong.longitude);

        float distance=l1.distanceTo(l2);
        String dist= String.format("%.0f", distance) +" M";


        if(distance>1000.0f)
        {
            distance=distance/1000.0f;
            dist= String.format("%.2f", distance) +" KM";
        }

        return dist;

    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

        }



        public void setTitle(String name) {
            TextView title = mView.findViewById(R.id.namatoko);
            title.setText(name);
        }

        public void setJarak(String jarak) {
            TextView jarakmap = mView.findViewById(R.id.jarak);
            jarakmap.setText(jarak);
        }
        
        public void setHarga(String harga) {
            TextView hargabarang = mView.findViewById(R.id.harga);
            hargabarang.setText(harga);
        }




    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

}
