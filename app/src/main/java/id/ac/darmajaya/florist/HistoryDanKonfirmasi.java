package id.ac.darmajaya.florist;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import id.ac.darmajaya.florist.Model.Florist;
import id.ac.darmajaya.florist.Model.Transaksi;

public class HistoryDanKonfirmasi extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FirestoreRecyclerAdapter<Transaksi, HistoryDanKonfirmasi.MyViewHolder> adapter;
    private ArrayList<String> transaksii = new ArrayList<>();
    private Dialog MyDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_transaksi_activity);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);

        FirebaseFirestore db = FirebaseFirestore.getInstance();



        starter();
    }


    public void starter(){

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        SharedPreferences sp = getSharedPreferences("your_prefs", Activity.MODE_PRIVATE);

         Query query = db.collection("transaksi");




   /*        newNoteRef.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
               @Override
               public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                   List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();// add check for each type of document...

                   System.out.println("lalala "+ documentSnapshots);


               }

           });*/

        Query transaksidb = db
                .collection("transaksi")
                .whereEqualTo("iduser", sp.getString("USERID", null));


        FirestoreRecyclerOptions<Transaksi> options = new FirestoreRecyclerOptions.Builder<Transaksi>()
                .setQuery(transaksidb, Transaksi.class)
                .build();



        adapter = new FirestoreRecyclerAdapter<Transaksi, HistoryDanKonfirmasi.MyViewHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull final HistoryDanKonfirmasi.MyViewHolder holder, final int position, @NonNull final Transaksi model) {

               DocumentReference data = FirebaseFirestore.getInstance().collection("toko").document(model.getIdtoko());

                data.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Florist data = documentSnapshot.toObject(Florist.class);
                        holder.setTitle(data.getNamatoko());
                        holder.setGambartoko(data.getFoto());

                    }
                });

                holder.setImage(model.getStatus());
                holder.setStat(model.getStatus());
                holder.setkonfirmasi(model.getStatus());
                String idtransaksi = adapter.getSnapshots().getSnapshot(position).getId();
                transaksii.add(idtransaksi);


                holder.konfirmasibtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), KonfirmasiTransaksi.class);
                        intent.putExtra("IDTRANSAKSI", transaksii.get(position));
                        startActivity(intent);
                    }
                });


                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), HistoryDetail.class);
                        intent.putExtra("IDTOKO", model.getIdtoko());
                        intent.putExtra("IDUSER", model.getIduser());
                        intent.putExtra("IDBARANG", model.getIdbarang());
                        intent.putExtra("STATUS", model.getStatus());
                        intent.putExtra("FOTOKARANGAN", model.getFotokarangan());
                        intent.putExtra("ALAMATTUJUAN", model.getAlamattujuan());
                        intent.putExtra("TANGGALPENGIRIMAN", model.getTanggalpengiriman());
                        intent.putExtra("DESKRIPSI", model.getDeskripsi());
                        intent.putExtra("FOTOBUKTI", model.getFototransaksi());
                        startActivity(intent);


                    }
                });

            }



            @NonNull
            @Override
            public HistoryDanKonfirmasi.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.history_transaksi_rcv, parent, false);


                return new HistoryDanKonfirmasi.MyViewHolder(view);
            }
        };

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        View mView;
        Button konfirmasibtn;

        public MyViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        /*    mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });*/
            konfirmasibtn = itemView.findViewById(R.id.konfirmasi);


        }


        public void setTitle(String name) {
            TextView title = mView.findViewById(R.id.namatoko);
            title.setText(name);
        }

        public void setStat(String status) {
            TextView statuss = mView.findViewById(R.id.status);
            

            if (status.equals("1")){
                statuss.setText("Transfer Belum Diverifikasi");

            }
            else if (status.equals("2")){
                statuss.setText("Pesanan Sedang Diproses");
            }

            else if (status.equals("3")){
                statuss.setText("Orderan Selesai");

            }
            else
            {
                statuss.setText("Belum Transfer");
            }


        }

        public void setkonfirmasi(String konfirmasi) {


            if (konfirmasi.equals("1")){
                konfirmasibtn.setBackgroundColor(getResources().getColor(R.color.yellow_600));
                konfirmasibtn.setEnabled(false);
            }
            else if (konfirmasi.equals("2")){
                konfirmasibtn.setBackgroundColor(getResources().getColor(R.color.blue_400));
                konfirmasibtn.setEnabled(false);

            }
            else if (konfirmasi.equals("3")){
                konfirmasibtn.setBackgroundColor(getResources().getColor(R.color.green_500));
                konfirmasibtn.setEnabled(false);
            }
            else
            {
                konfirmasibtn.setBackgroundColor(getResources().getColor(R.color.red_400));

            }
            
        }


        public void setImage(String image) {
            ImageView foto = mView.findViewById(R.id.gambarverify);


            if (image.equals("1")){
                foto.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.yellow_600));
            }
            else if (image.equals("2")){
                foto.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.blue_400));
            }

            else if (image.equals("3")){
                foto.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green_500));

            }
            else
            {
                foto.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.red_400));
            }


        }

        public void setGambartoko(String image) {
            ImageView foto = mView.findViewById(R.id.gambartoko);
            Picasso.get()
                    .load(image)
                    .into(foto);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



}
