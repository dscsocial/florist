package id.ac.darmajaya.florist;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import id.ac.darmajaya.florist.R;

public class DetailProdukActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_produk_activity);

        ImageView gambar = (ImageView) findViewById(R.id.gambar);
        TextView deskripsi = (TextView) findViewById(R.id.deskripsi);


        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("NAMA"));

        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Picasso.get()
                .load(getIntent().getStringExtra("GAMBAR"))
                .into(gambar);

        deskripsi.setText(getIntent().getStringExtra("DESKRIPSI"));




        FloatingActionButton btnshop = (FloatingActionButton) findViewById(R.id.btnshop);
        btnshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PemesananFormActivity.class);

                startActivity(intent);
            }
        });






    }

}
