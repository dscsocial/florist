package id.ac.darmajaya.florist;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import es.dmoral.toasty.Toasty;
import id.ac.darmajaya.florist.Model.User;


public class RegistationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText ETemail, ETpassword, ETnama, ETalamat, ETnotelp;
    private FirebaseFirestore db;
    Button submit;
    public TextInputLayout inputLayoutEmail, inputLayoutPassword, inputLayoutNama, inputLayoutAlamat, inputLayoutNotelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.register_activity);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();


        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputLayoutNama = (TextInputLayout) findViewById(R.id.input_layout_nama);
        inputLayoutAlamat = (TextInputLayout) findViewById(R.id.input_layout_alamat);
        inputLayoutNotelp = (TextInputLayout) findViewById(R.id.input_layout_notelp);

        ETemail = (EditText) findViewById(R.id.edmail);
        ETpassword =(EditText) findViewById(R.id.edpassword);
        ETnama =(EditText) findViewById(R.id.ednama);
        ETalamat =(EditText) findViewById(R.id.edalamat);
        ETnotelp =(EditText) findViewById(R.id.ednotelp);
        submit = (Button) findViewById(R.id.submit);

        ETemail.addTextChangedListener(new MyTextWatcher(ETemail));
        ETpassword.addTextChangedListener(new MyTextWatcher(ETpassword));
        ETnama.addTextChangedListener(new MyTextWatcher(ETnama));
        ETalamat.addTextChangedListener(new MyTextWatcher(ETalamat));
        ETnotelp.addTextChangedListener(new MyTextWatcher(ETnotelp));



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

    }

    private class MyTextWatcher implements TextWatcher {
            private View view;

            private MyTextWatcher(View view) {
                this.view = view;
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
                switch (view.getId()) {

                    case R.id.edmail:
                        validateEmail();
                        break;
                    case R.id.edpassword:
                        validatePassword();
                        break;
                }
            }
    }

    private void submitForm() {
        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        if (!validateNama()) {
            return;
        }

        if (!validateAlamat()) {
            return;
        }

        if (!validateNotelp()) {
            return;
        }

        String email = ETemail.getText().toString();
        String password = ETpassword.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            addtodb();
                            Toasty.success(RegistationActivity.this, "Pendaftaran Sukses!", Toast.LENGTH_SHORT, true).show();

                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(RegistationActivity.this, "Registrasi Gagal", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private boolean validateEmail() {
        String email = ETemail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError("Error");
            requestFocus(ETemail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        String password = ETpassword.getText().toString().trim();

        if (password.isEmpty() || !isPasswordValid(password)) {
            inputLayoutPassword.setError("Password tidak boleh < 4");
            requestFocus(ETpassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateNama() {
        String nama = ETnama.getText().toString().trim();

        if (nama.isEmpty()) {
            inputLayoutNama.setError("Error");
            requestFocus(ETnama);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAlamat() {
        String alamat = ETalamat.getText().toString().trim();

        if (alamat.isEmpty()) {
            inputLayoutAlamat.setError("Error");
            requestFocus(ETalamat);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateNotelp() {
        String notelp = ETnotelp.getText().toString().trim();

        if (notelp.isEmpty()) {
            inputLayoutNotelp.setError("Error");
            requestFocus(ETnotelp);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public void addtodb(){

        String userid = mAuth.getUid();
        String email = ETemail.getText().toString();
        String password = ETpassword.getText().toString();
        String nama = ETnama.getText().toString();
        String alamat = ETalamat.getText().toString();
        String notelp = ETnotelp.getText().toString();
        String notoken = "";

        User user = new User(email, password, nama, alamat, notelp, notoken);

        db.collection("user").document(userid).set(user);

    }


}
